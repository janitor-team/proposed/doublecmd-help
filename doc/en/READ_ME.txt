This file contains general instructions and convention about HTML help file of DC.
==================================================================================

When using a link to something external to Double Commander help, set the target to be a new window using target="_blank" like in this example:
  <a title="Double Commander" href="http://doublecmd.sourceforge.net/" target="_blank">Homepage</a>

To be more friendly with the user, to make us as the same level as the user, let's talk using "WE" instead of "YOU".
The user will feel that we're working together.