doublecmd-help (0.9.7-1) unstable; urgency=medium

  * New upstream release
  * Update debian/copyright
  * Bump Standards-Version to 4.4.1, no changes

 -- Graham Inggs <ginggs@debian.org>  Thu, 16 Jan 2020 13:42:00 +0000

doublecmd-help (0.9.3-2) unstable; urgency=medium

  * Install images from pixmaps/common to docs/*/images
    as these are now also needed by Russian help
  * Drop privacy-breach-w3c-valid-html.patch and use sed -z instead
  * Switch to debhelper 12
  * Set Rules-Requires-Root: no
  * Upload to unstable

 -- Graham Inggs <ginggs@debian.org>  Thu, 04 Jul 2019 20:48:58 +0000

doublecmd-help (0.9.3-1) experimental; urgency=medium

  * New upstream release
  * Refresh privacy-breach-w3c-valid-html.patch

 -- Graham Inggs <ginggs@debian.org>  Thu, 30 May 2019 09:11:57 +0000

doublecmd-help (0.9.1-1) unstable; urgency=medium

  * New upstream release
  * Run OptiPNG without -fix option again

 -- Graham Inggs <ginggs@debian.org>  Sun, 24 Feb 2019 07:14:38 +0000

doublecmd-help (0.9.0-1) unstable; urgency=medium

  * New upstream release
  * Refresh privacy-breach-w3c-valid-html.patch
  * Run OptiPNG with -fix option for recoverable errors in images
  * Bump Standards-Version to 4.3.0, no changes
  * Mark help packages Multi-Arch: foreign as per Multiarch hinter

 -- Graham Inggs <ginggs@debian.org>  Wed, 13 Feb 2019 16:13:48 +0000

doublecmd-help (0.8.3-1) unstable; urgency=medium

  * New upstream release
  * Refresh privacy-breach-w3c-valid-html.patch
  * Update Vcs-* URIs for move to salsa.debian.org
  * Turn debhelper up to 11
  * Update debian/copyright
  * Bump Standards-Version to 4.1.4, no changes

 -- Graham Inggs <ginggs@debian.org>  Mon, 11 Jun 2018 13:07:08 +0000

doublecmd-help (0.8.0-1) unstable; urgency=medium

  * New upstream release
  * Update debian/watch for move to github
  * Update debian/copyright and Uploaders
  * Switch to debhelper 10
  * Fix privacy-breach-w3c-valid-html
  * Use secure URIs for VCS fields
  * Bump Standards-Version to 4.1.2, no changes

 -- Graham Inggs <ginggs@debian.org>  Tue, 19 Dec 2017 13:49:09 +0000

doublecmd-help (0.6.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Graham Inggs <graham@nerve.org.za>  Wed, 13 May 2015 12:16:54 +0200

doublecmd-help (0.6.0-1) experimental; urgency=medium

  * New upstream release
  * Update d/doublecmd-help-uk.*: directory uk_UA renamed to uk
  * Optimize *.png screenshots with OptiPNG
  * Update d/copyright:
    - update copyright years
    - use dh_make copyright templates
  * Bump Standards-Version to 3.9.6, no changes

 -- Graham Inggs <graham@nerve.org.za>  Mon, 16 Feb 2015 14:05:56 +0200

doublecmd-help (0.5.5-2) unstable; urgency=medium

  * Update d/control for Pascal Packaging Team maintenance
  * Install files using dh_install
  * Bump Standards-Version to 3.9.5, no changes
  * Convert *.html and *.css from DOS to Unix format

 -- Graham Inggs <graham@nerve.org.za>  Thu, 15 May 2014 18:12:24 +0200

doublecmd-help (0.5.5-1) unstable; urgency=low

  * Initial release (Closes: #720325)

 -- Graham Inggs <graham@nerve.org.za>  Tue, 20 Aug 2013 15:33:48 +0200
